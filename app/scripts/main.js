'use strict';

// ready
$(document).ready(function() {

    // anchor
    $(".anchor").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top - 100}, 1000);
    });
    // anchor

    // adaptive menu
    $('.main-nav__toggle--js').click(function () {
        $(this).toggleClass('open').next().toggleClass('collapse');
    });
    // adaptive menu

    // $('#form-id .control').change(function() {
    //     validateForm('#form-id');
    // });
    // $('#reg-id .control').change(function() {
    //     validateForm('#reg-id');
    // });
    $('#rest-id .control').change(function() {
        validateForm('#rest-id');
    });
    $('#regin-id .control').change(function() {
        validateForm('#regin-id');
    });
    $('#reginl-id .control').change(function() {
        validateForm('#reginl-id');
    });
    function validateForm(form) {
      var isValid = true;
      $(form + ' .btn').prop('disabled', true);
      $(form + ' .control').each(function() {
        if ( $(this).val() === '' )
          isValid = false;
      });
      if(isValid) {
        $(form + ' .btn').prop('disabled', false);
      }
    }


    // animation
    $('.animated').appear(function() {
        var elem = $(this);
        var animation = elem.data('animation');

        if ( !elem.hasClass('visible') ) {
            var animationDelay = elem.data('animation-delay');
            if ( animationDelay ) {
                setTimeout(function(){
                    elem.addClass( animation + " visible" );
                }, animationDelay);

            } else {
                elem.addClass( animation + " visible" );
            }
        }
    });
    // animation

    // mask phone {maskedinput}
    //$("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone

    $(".slider-main__item").hover(
      function() {
        var gold = $(this).data('gold');
        var silver = $(this).data('silver');
        $('.slider-main__item-txt').html('<div class="text--warning text__sm"><div class="text__md">'+gold+'%</div>Gold</div><div class="text--white text__sm"><div class="text__md">'+silver+'%</div>Silver</div>');
      }
    );

    // slider
    $('.slider-main').slick({
       slidesToShow: 9,
       // slidesToScroll: 13,
       variableWidth: true,
       arrows: false,
       centerMode: false,
       responsive: [
         {
            breakpoint: 720,
            settings: {
              slidesToShow: 6,
              slidesToScroll: 6,
              centerMode: false
            }
         }
       ]
    });
    $('.slider-main--c').slick({
       slidesToShow: 9,
       // slidesToScroll: 13,
       variableWidth: true,
       arrows: false,
       centerMode: true,
       responsive: [
         {
            breakpoint: 720,
            settings: {
               slidesToShow: 6,
               slidesToScroll: 6,
               centerMode: false
            }
         }
       ]
    });
    $('.slider-arrow-left').click(function (e) {
      $(".slider-main").slick('slickNext');
      $(".slider-main--c").slick('slickNext');
    });
    $('.slider-arrow-right').click(function (e) {
      $(".slider-main").slick('slickPrev');
      $(".slider-main--c").slick('slickPrev');
    });
    $('.goods-block').slick({
        mobileFirst: true,
        dots: true,
        arrows: false,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 720,
                settings: 'unslick'
            }
        ]
    });
    $('.reviews-block').slick({
       slidesToShow: 1,
       adaptiveHeight: true,
       arrows: true,
       dots: true,
       appendArrows: ".slider-arrows",
       nextArrow: '<div class="slider-arrows-left slick-arrow"><i class="icon-next"></i></div>',
       prevArrow: '<div class="slider-arrows-right slick-arrow"><i class="icon-prev"></i></div>',
       responsive: [
           {
               breakpoint: 720,
               settings: {
                 asNavFor: '.slider-nav',
                 arrows: false,
                 dots: false
               }
           }
       ]
    });
    $('.slider-nav').slick({
       slidesToShow: 1,
       slidesToScroll: 1,
       variableWidth: true,
       asNavFor: '.reviews-block',
       arrows: false,
       centerMode: true,
       focusOnSelect: true
    });
    // slider

    // select {select2}
    //$('select').select2({});
    // select

    // popup {magnific-popup}
    $('.popup').magnificPopup({
  		type: 'inline',

  		fixedContentPos: false,
  		fixedBgPos: true,

  		overflowY: 'auto',
  		removalDelay: 300,
      closeMarkup: '<a class="popup-close popup-modal-dismiss" href="#"><i class="icon-close"></i></a>',
  		mainClass: 'my-mfp-zoom-in'
  	});
  	$(document).on('click', '.popup-close', function (e) {
  		e.preventDefault();
  		$.magnificPopup.close();
  	});
    // popup

    $('.accordion__title').click(function() {
        $(this).parent().siblings('.accordion__item').find('.accordion__body').slideUp('fast');
        $(this).next('.accordion__body').slideToggle('fast');
    });

    $(".slider-main__item").click(
      function() {
        $('.slider-main__item-txt').html('');
        var gold = $(this).data('gold');
        var silver = $(this).data('silver');
        $('.slider-main__item-txt').html('<div class="text--warning text__sm"><div class="text__md">'+gold+'%</div>Gold</div><div class="text--white text__sm"><div class="text__md">'+silver+'%</div>Silver</div>');
      }
    );
});
// ready


// load
$(document).load(function() {});
// load

// scroll
$(window).scroll(function() {});
// scroll

// mobile sctipts
var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
if (screen_width <= 767) {

}
// mobile sctipts
